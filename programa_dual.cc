#include <iostream>
#include "dual.h"
#include <cmath>
#include <complex>

using namespace std;

template <class S=int>
Dual<S> f_cuadrado(Dual<S> d)
{
  Dual<S> dd = d*d;
  return d.get_dual();
}
//                      COSENO
template <class S>
Dual<S> cos(Dual<S> d)
{
  S real = cos(d.get_real());
  S dual = -1*d.get_dual()*sin(d.get_real());
  S dual2 = (-0.5*cos(d.get_real())*(d.get_dual()*d.get_dual()))-(d.get_dual2()*sin(d.get_real()));
  return Dual<S>(real, dual, dual2);
}
//                          SENO
template <class S>
Dual<S> sin(Dual<S> d){
  S real = sin(d.get_real());
  S dual = d.get_dual()*cos(d.get_real());
  S dual2 = (-0.5*sin(d.get_real())*(d.get_dual()*d.get_dual()))+(d.get_dual2()*cos(d.get_real()));
  return Dual<S>(real, dual, dual2);
}
//                          EXP
template <class S>
Dual<S> exp(Dual<S> d){
  S real = exp(d.get_real());
  S dual = d.get_dual()*exp(d.get_real());
  S dual2 = 0.5*(exp(d.get_real())*(d.get_dual()*d.get_dual()))+(d.get_dual2()*exp(d.get_real()));
  return Dual<S>(real, dual, dual2);
}
//                         LOG
template <class S>
Dual<S> log(Dual<S> d){
  S real = log(d.get_real());
  S dual = d.get_dual()/d.get_real();
  S dual2 = (-0.5*(d.get_dual()*d.get_dual())/(d.get_real()*d.get_real()))+(d.get_dual2()/d.get_real());
  return Dual<S>(real, dual, dual2);
}
//                         COSEC
template <class S>
Dual<S> cosec(Dual<S> d){
  S real = 1.0/sin(d.get_real());
  S dual = -1.*d.get_dual()*cos(d.get_real())*pow(sin(d.get_real()),-2.);
  S dual2 = -0.5*pow(sin(d.get_real()),-1)+2.*pow(sin(d.get_real()),-3)*pow(d.get_dual(),2) + -1.*d.get_dual2()*cos(d.get_real())*pow(sin(d.get_real()),-2);
  return Dual<S>(real, dual, dual2);
}
//                        TAN
template <class S>
Dual<S> tan(const Dual<S> d){
  S real = tan(d.get_real());
  S dual = d.get_dual()/(cos(d.get_real())*cos(d.get_real()));
  S dual2 = pow(d.get_dual(),2)*sin(d.get_real())*pow(cos(d.get_real()),-3)+ d.get_dual2()/(cos(d.get_real())*cos(d.get_real()));
  return Dual<S>(real, dual, dual2);
}

int main()
{

  Dual<complex<double>> a1(1.,1.);
  Dual<complex<double>> a2(1.,0,1.);
  Dual<complex<double>> b1(-1.,1.);
  Dual<complex<double>> b2(-1.,0,1.);
  Dual<double> c(3.14159,1.,0);

  
  cout << sin(log(cosec(exp(tan(a1))))).get_dual() << endl;
  cout << sin(log(cosec(exp(tan(a2))))).get_dual2() << endl;
  cout << sin(log(cosec(exp(tan(a1))))).get_dual() << endl;
  cout << sin(log(cosec(exp(tan(a1))))).get_dual2() << endl;
  cout << cos(c).get_dual() << endl;
  cout << sin(c).get_dual() << endl;
}
