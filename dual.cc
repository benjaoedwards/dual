#include <iostream>
#include "dual.h"

using namespace std;


Dual::Dual(double x,double y)
{
	real = x;
	dual = y;

}

Dual::Dual()
{}

void Dual::set_real(double x)
{
	real = x;
}

void Dual::set_dual(double x)
{
	dual = x;
}

double Dual::get_real()
{
	return real;
}

double Dual::get_dual()
{
	return dual;
}

Dual::Dual (const Dual & d)
{
	real = d.real;
	dual = d.dual;
}

Dual operator + (Dual d1,Dual d2)
{
	return Dual(d1.get_real() + d2.get_real(),d1.get_dual()+d2.get_dual());
}

Dual operator * (Dual d1,Dual d2)
{
	return Dual((d1.get_real() * d2.get_real()) ,(d1.get_real()*d2.get_dual())+(d2.get_real()*d1.get_dual()));
}

ostream & operator << (ostream & os ,Dual c)
{
	os << c.get_real() << " + " << c.get_dual() << "e";
	return os;

}

