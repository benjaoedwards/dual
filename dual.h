
#ifndef DUAL_H
#define DUAL_H
 
using namespace std;

template <class T>
class Dual
{
private:
	T real;
	T dual;
	T dual2;
	T dual3;
	
public:
	Dual();
	Dual(T,T=0,T=0,T=0);
	Dual (const Dual &);
	void set_real(T);
	void set_dual(T);
	void set_dual2(T);
	void set_dual3(T);
	T get_real() const;
	T get_dual() const;
	T get_dual2() const;
	T get_dual3() const;
};
#endif

template <class T>
Dual<T>::Dual(T x,T d1,T d2, T d3)
{
	real = x;
	dual = d1;
	dual2 = d2;	
	dual3 = d3;
}

template <class T>
Dual<T>::Dual()
{}

template <class T>
void Dual<T>::set_real(T x)
{
	real = x;
}

template <class T>
void Dual<T>::set_dual(T x)
{
	dual = x;
}

template <class T>
void Dual<T>::set_dual2(T x)
{
	dual2 = x;
}

template <class T>
void Dual<T>::set_dual3(T x)
{
	dual3 = x;
}

template <class T>
T Dual<T>::get_real() const
{
	return real;
}

template <class T>
T Dual<T>::get_dual() const
{
	return dual;
}

template <class T>
T Dual<T>::get_dual2() const
{
	return dual2;
}

template <class T>
T Dual<T>::get_dual3() const
{
	return dual3;
}

template <class T>
Dual<T>::Dual (const Dual<T> & d)
{
	real = d.real;
	dual = d.dual;
	dual2 = d.dual2;
	dual3 = d.dual3;

template <class T>
Dual<T> operator + (Dual<T> d1,Dual<T> d2)
{
  return Dual<T>(d1.get_real() + d2.get_real(), d1.get_dual() + d2.get_dual(), d1.get_dual2() + d2.get_dual2(), d1.get_dual3() + d2.get_dual3());
}

template <class T>
Dual<T> operator - (Dual<T> d1,Dual<T> d2)
{
  return Dual<T>(d1.get_real() - d2.get_real(), d1.get_dual() - d2.get_dual(), d1.get_dual2() - d2.get_dual2(), d1.get_dual3() - d2.get_dual3());
}

template <class T>
Dual<T> operator * (Dual<T> d1,Dual<T> d2)
{
  return Dual<T>((d1.get_real() * d2.get_real()) ,(d1.get_real()*d2.get_dual())+(d2.get_real()*d1.get_dual()),(d2.get_real()*d1.get_dual2()+d2.get_dual()*d1.get_dual()));
}

template <class T>
ostream & operator << (ostream & os ,Dual<T> c)
{
  os << c.get_real();
  if (c.get_dual() >= T(0)) os << " +";
  os << c.get_dual() << "e";
  if (c.get_dual2() >= T(0)) os << " +";
  os << c.get_dual2() << "e^2";
  if (c.get_dual3() >= T(0)) os <<  " +";
  os << c.get_dual3() << "e^3" << endl;
  return os;

}

